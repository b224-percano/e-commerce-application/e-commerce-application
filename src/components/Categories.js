import { Card, Col } from 'react-bootstrap';

export default function Categories({categoryProp}){

    const { title } = categoryProp

    return (
        <Col lg={1} xs={3} md={2} className="text-center p-0 m-2"> 
            <Card className='p-1'>
                <Card.Link href="#" className='text-black text-decoration-none'>
                    <Card.Text>{title}</Card.Text>
                </Card.Link>
            </Card>
        </Col>
    )
};
