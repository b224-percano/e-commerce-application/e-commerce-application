import { Button, Card, Container } from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';


export default function AllProductsTable({productProp}){

    const { _id, name, description, price, stock, sold, isActive } = productProp;

     const archive = () => {

            fetch(`${process.env.REACT_APP_API_URL}/products/update/${_id}`, {
                method: "PATCH",
                headers: {
                    'Content-Type': "application/json",
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({
                    isActive: false
                })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data)
                if(data === true){
                    Swal.fire({
                        title: "Successully Archived Product",
                        icon: "success",
                        text: `You have successfully Archived product ${name}`
                    })


                }else{
                    Swal.fire({
                        title: "Something Went Wrong",
                        icon: "error",
                        text: `Unexpected error please try again later!`
                    })
                }
            })
            .catch(error => console.log(error))
        };

        const unArchive = (e) => {

            fetch(`${process.env.REACT_APP_API_URL}/products/update/${_id}`, {
                method: "PATCH",
                headers: {
                    'Content-Type': "application/json",
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({
                    isActive: true
                })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data)
                if(data === true){
                    Swal.fire({
                        title: "Successully Unarchived Product",
                        icon: "success",
                        text: `You have successfully Unarchived product ${name}`
                    })


                }else{
                    Swal.fire({
                        title: "Something Went Wrong",
                        icon: "error",
                        text: `Unexpected error please try again later!`
                    })
                }
            })
            .catch(error => console.log(error))
        };

    return(
        <tbody>
            <tr className="text-center align-items-center">
                <td><p>{name}</p></td>
                <td><p>&#8369;{price}</p></td>
                <td><p>{stock}</p></td>
                <td><p>{sold}</p></td>
                <td><p>
                    {(isActive ? "Available" 
                    : 
                    "Unavailable")}
                </p></td>
                <td>
                    <div className='d-block'>
                        <Button as={Link} to={'/update/'+_id}>Update</Button>
                    </div>

                    <div className='mt-1'>
                    {(isActive) ? 
                         <Button type="button" variant='danger' onClick={archive}>Archive</Button>
                     :
                         <Button type="button" variant='success' onClick={e => unArchive(e)} >Unarchive</Button>
                     } 

                    </div>
                </td>
            </tr>
        </tbody>
    )
}
