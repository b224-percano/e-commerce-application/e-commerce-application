import { useState, useEffect, useContext } from "react";
import { Card, Button, Row, Col, InputGroup, Form, Container, Modal  } from 'react-bootstrap';
import { useParams, useNavigate, Link } from "react-router-dom"; 
import Swal from 'sweetalert2';
import Order from "../components/Order";
import UserContext from '../UserContext';

export default function ProductView(){
    
    const { productId } = useParams()
    const { user } = useContext(UserContext)

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [stock, setStock] = useState(0);
    const [sold, setSold] = useState(0);
    const [qty, setQty] = useState(1);


    const order = {productId, name, qty}
    console.log(order)


    function addQty(){
        setQty(oldQty => {
            return (oldQty === stock ? oldQty : oldQty + 1)
        })
    };

    function substractQty(){
        setQty(oldQty => {
            return (oldQty ===1 ? oldQty : oldQty - 1)
        })
    };

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/find/${productId}`)
        .then(res => res.json())
        .then(data => {
            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);
            setStock(data.stock);
            setSold(data.sold);


        })
    },[])

    return(
    	<Container>
            	<Row>
                    <Col>
                       <div xs={12} lg={2} sm={12} style={{width: '30%'}} className="p-0 mx-auto align-items-center">
                            <Card border='dark' className="mt-5 text-center">
                                <Card.Body className='mt-3'>
                                    <Card.Title>{name}</Card.Title>
                                    <Card.Text>{description}</Card.Text>
                                    <Card.Body>
                                        <div className='d-flex'>
                                            <Card.Text>Price:</Card.Text>
                                            <Card.Title className='ms-3 text-danger'>&#8369;{price}.00</Card.Title>
                                        </div>
                                        <div className='d-flex'>
                                            <Card.Text>Stock:</Card.Text>
                                            <Card.Text className='ms-3'>{stock}</Card.Text>
                                        </div>
                                        <div className='d-flex'>
                                            <Card.Text className='pt-2'>Quantity:</Card.Text>
                                            <InputGroup className="ms-3">
                                                <Button onClick={substractQty} variant="dark">-</Button>
                                                <Form.Control className="text-center"
                                                placeholder={qty}
                                                value={qty}
                                                onChange={e => setQty(e.target.value)}
                                                disabled
                                                />
                                                <Button onClick={addQty} variant="dark">+</Button>
                                            </InputGroup>
                                        </div>
                                        <div className='mt-5'>
                                            <Order orderProp={order} />
                                        </div>
                                    </Card.Body>
                                </Card.Body>
                            </Card>
                        </div>
                    </Col>
                </Row>
        </Container>
    )
};


