import { Col, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom'


export default function ProductCard({productProp}){

	 const {name, price, sold, _id} = productProp

	 return(
	 	<>
		 	<Col className='p-0 mt-1 mb-2'lg={2} md={3} xs={6}>
		        <Card className='m-1'>
		            <Card.Link className='text-black text-decoration-none' as={Link} to={`/products/${_id}`}>
		                <Card.Body>
		                    <Card.Title className='mb-3'>{name}</Card.Title>
		                    <Card.Text className='cardPrice'>&#8369;{price}.00</Card.Text>
		                </Card.Body>
		            </Card.Link>
		        </Card>
	        </Col>
	 	</>
	 )
}