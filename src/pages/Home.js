import { useState, useEffect } from 'react';
import { Row, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Banner from '../components/Banner';
import categoriesData from '../data/categoriesData';
import ProductCard from '../components/ProductCard';
import Categories from '../components/Categories';
import Footer from '../components/Footer'



export default function Home(){
    
    const [hotProducts, setHotProducts] = useState([]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/hotProducts`)
        .then(res => res.json())
        .then(data => {
            setHotProducts(data.map(product => {
                return (
                    <ProductCard key={product._id} productProp={product} />
                )
            }))
        })
        .catch(error => console.log(error))
    }, []);



    const categories = categoriesData.map(category => {
        return (
            <Categories key={category.title} categoryProp={category} />
        )
    })


    return (
        <>
            <Banner />
            <h1 className="productH1 text-center mt-5">Best Seller</h1>
            <Row className='justify-content-center mt-1'>
                {hotProducts}
            </Row>
                <div className='text-center mt-3'>
                    <Button as={Link} to='/products' variant='outline-dark'>All Products</Button>
                </div>
            <Footer />
        </>
    )
}

