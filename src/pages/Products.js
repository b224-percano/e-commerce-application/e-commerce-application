import { useState, useEffect } from 'react';
import { Container, Row } from 'react-bootstrap';
import ProductCard from '../components/ProductCard';



export default function Products(){
    
    const [products, setProducts] = useState([]);
    const [hotProducts, setHotProducts] = useState([]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/`)
        .then(res => res.json())
        .then(data => {
            setProducts(data.map(product => {
                return (
                    <ProductCard key={product._id} productProp={product} />
                )
            }))
        })
        .catch(error => console.log(error))
    }, []);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/hotProducts`)
        .then(res => res.json())
        .then(data => {
            setHotProducts(data.map(product => {
                return (
                    <ProductCard key={product._id} productProp={product} />
                )
            }))
        })
        .catch(error => console.log(error))
    }, []);


    return (
    	<>
	        <h1 className="productH1 text-center mt-5">Best Seller</h1>
	            <Container>
                    <Row className='mt-3 justify-content-center'>
                        {hotProducts}
                    </Row>
                </Container>
	        <h1 className="productH1 text-center mt-5">All Products</h1>
                <Container className='d-flex'>
                    <Row className='mt-3 justify-content-center' >
                    {products}
                    </Row>
                </Container>
        </>
    )
}


