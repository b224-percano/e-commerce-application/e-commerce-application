import { useContext } from 'react';
import { Button, Container, Form, Nav, Navbar, NavDropdown, Offcanvas } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
import '../App.css';

export default function AppNavbar(){

  const { user } = useContext(UserContext);

	return(
		<Navbar bg="dark" variant="dark" expand="lg" sticky="top" className="nav">
      	<Container fluid className="no-gutters">
        <Navbar.Brand as={Link} to="/" className="shopName">Bits N Bytes</Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="navOptions me-auto my-2 my-lg-0 mx-auto"
            style={{ maxHeight: '100px' }}
            navbarScroll
          >
             {
              (user.isAdmin === true) ?
                <>
                  <Nav.Link as={Link} to="/admin" >My Profile</Nav.Link>
                  <Nav.Link as={Link} to="/admin/addProduct" >Add Products</Nav.Link>
                  <Nav.Link as={Link} to="/admin/allProducts">All Products</Nav.Link>
                  {/* <Nav.Link as={Link} to="/">Home</Nav.Link>
                  <Nav.Link as={Link} to="/Products">Products</Nav.Link> */}
                </>
                :
                <>
                  <Nav.Link as={Link} to="/">Home</Nav.Link>
                  <Nav.Link as={Link} to="/Products">Products</Nav.Link>
                </>
             }
             {
              (user.id !== null) ?
                  <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
                :
                <>
                  <Nav.Link as={Link} to="/register">Register</Nav.Link>
                  <Nav.Link as={Link} to="/login">Login</Nav.Link>
                </>
             }
          </Nav>
          <Form className="d-flex" id="searchBar">
            <Form.Control
              type="search"
              placeholder="Search"
              className="me-2"
              aria-label="Search"
            />
            <Button variant="outline-success">Search</Button>
          </Form>
        </Navbar.Collapse>
      </Container>
    </Navbar>
	);
}