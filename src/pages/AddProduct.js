import { useState, useEffect, useContext } from 'react';
import { Row, Col, Form, Button, Container } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import Banner from '../components/Banner';


export default function AddProduct(){
    
    const { user } =useContext(UserContext);
    const navigate = useNavigate()
    
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [category, setCategory] = useState("");
    const [price, setPrice] = useState(0);
    const [stock, setStock] = useState(1);
    const [isBtnActive, setIsBtnActive] =useState(false);

    function addProduct(e){
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
            method: "POST",
            headers: {
                'Content-Type': "application/json",
                Authorization : `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                category: category,
                price: price,
                stock: stock
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                Swal.fire({
                    title: "Successully Add Product",
                    icon: "success",
                    text: `You have successfully added product ${name}`
                })

                setName("");
                setDescription("");
                setCategory("");
                setPrice(0);
                setStock(1);
                setIsBtnActive(false);

                navigate('/admin/addProduct')
            } else if(data === false){
                Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
    
            }else {
                Swal.fire({
                    title: "Duplicate Product Name",
                    icon: "error",
                    text: "Product name is already exist please try another product name"
				})
            }
        })
        .catch(error => console.log(error))
    };

    useEffect(() => {
        if(name !== "" && description !== "" && category !== ""){
            setIsBtnActive(true)
        }else{
            setIsBtnActive(false)
        }
    }, [name, description, category])

    
     return (
            (user.isAdmin === false) ?
                <Navigate to="/" />
            :
            <>
                <Row>
                    <Col className='p-2'>
                        <Form onSubmit={e => addProduct(e)} className="addProdForm p-3">
                            <h1 className="text-center addProdWord">Add Product</h1>
                            <Container>
                            <Form.Group className="mb-3" controlId="addProductName">
                                <Form.Label>Product Name</Form.Label>
                                <Form.Control 
                                type="text" 
                                placeholder="Enter Product Name"
                                value={name}
                                onChange={e => setName(e.target.value)}
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="addProductDescription">
                                <Form.Label>Product Description</Form.Label>
                                <Form.Control 
                                type="text" 
                                placeholder="Enter Product Description" 
                                value={description}
                                onChange={e => setDescription(e.target.value)}
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="addProductDescription">
                                <Form.Label>Product Type</Form.Label>
                                <Form.Select 
                                aria-label="Default select example"
                                value={category} 
                                onChange={e => setCategory(e.target.value)}
                                >
                                    <option>Select Product Type</option>
                                    <option value="Console Game">Console Game</option>
                                    <option value="PC Peripherals">PC Peripherals</option>
                                    <option value="Handheld Device">Handheld Device</option>
                                    <option value="Playstation">Playstation</option>
                                    <option value="Xbox">Xbox</option>
                                </Form.Select>
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="addProductPrice">
                                <Form.Label>Price</Form.Label>
                                <Form.Control 
                                type="number" 
                                placeholder="Product Price"
                                value={price} 
                                onChange={e => setPrice(e.target.value)}
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="addProductStock">
                                <Form.Label>Stock</Form.Label>
                                <Form.Control 
                                type="number" 
                                placeholder="Product Stocks"
                                value={stock} 
                                onChange={e => setStock(e.target.value)} 
                                />
                            </Form.Group>

                            <div>
                                {(isBtnActive ? 
                                    <Button variant="success" type="submit">Submit</Button>
                                :
                                    <Button variant="success" disabled >Submit</Button>
                                )}
                            </div>
                            </Container>
                        </Form>
                    </Col>
                </Row>
            </>
        )
    }
