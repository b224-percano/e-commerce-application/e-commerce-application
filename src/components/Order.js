import { useState } from "react";
import { Button, Modal, Form, Card } from 'react-bootstrap';
import { useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2'

export default function Order({orderProp}) {

    const navigate = useNavigate()
    const {productId, name, qty} = orderProp
    const [address, setAddress] = useState("");
    const [mobileNo, setMobileNo] = useState("");
    
    const [show, setShow] = useState(false);
  
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const addOrder = (e) => {
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/orders/createOrder/${productId}`, {
            method: "POST",
            headers: {
                'Content-Type': "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                quantity: qty,
                address: address,
                contactNo: mobileNo
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                Swal.fire({
					title: "Successully Order",
					icon: "success",
					text: `You have successfully ordered product ${name}`
				})
                setAddress("");
                setMobileNo("");
                navigate('/products')
            }else{
                Swal.fire({
					title: "Something Went Wrong",
					icon: "error",
					text: `Unexpected error please try again later!`
				})
            }
        })
        .catch(error => console.log(error))
    }


    return (
      <>
        <Button className=' btn-order ms-3' variant='success' onClick={handleShow}>Buy Now</Button>
  
        <Modal
          show={show}
          onHide={handleClose}
          backdrop="static"
          keyboard={false}
        >
          <Modal.Header closeButton>
            <Modal.Title>Order Product</Modal.Title>
          </Modal.Header>
          <Modal.Body>
          <Form onSubmit={e => addOrder(e)}>
                <Form.Group className="mb-3" controlId="orderedProduct">
                    <Card className="text-center">
                        <Card.Title>{name}</Card.Title>
                    </Card>
                </Form.Group>

                <Form.Group className="mb-3" controlId="orderQty">
                    <Form.Label>Quantity</Form.Label>
                    <Form.Control type="text" placeholder={qty} disabled/>
                </Form.Group>
                
                <Form.Group className="mb-3" controlId="userAddress">
                    <Form.Label>Address</Form.Label>
                    <Form.Control 
                    type="text" 
                    placeholder="Your Address"
                    value={address}
                    onChange={e => setAddress(e.target.value)} 
                    />
                </Form.Group>

                <Form.Group className="mb-3" controlId="userMobileNo">
                    <Form.Label>Mobile Number</Form.Label>
                    <Form.Control 
                    type="text" 
                    placeholder="Your Mobile Number" 
                    value={mobileNo}
                    onChange={e => setMobileNo(e.target.value)}
                    />
                </Form.Group>
                <div className="text-center">
                    <Button className="me-2" variant="danger" onClick={handleClose}>Close</Button>
                    {(mobileNo.length === 11 && address !== "") ? 
                        <Button type="submit" variant="success">Buy it now!</Button>
                    :
                        <Button  variant="success">Buy</Button>
                    }
                </div>
            </Form>
          </Modal.Body>
        </Modal>
      </>
    );
}
