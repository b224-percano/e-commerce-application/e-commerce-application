import { useState, useEffect, useContext } from 'react';
import { Row, Col, Form, Button, Container } from 'react-bootstrap';
import { Navigate, useNavigate, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function UpdateProduct(){

        const { user } =useContext(UserContext);
        const { productId } = useParams()
        const navigate = useNavigate()

        const [name, setName] = useState("");
        const [description, setDescription] = useState("");
        const [category, setCategory] = useState("");
        const [price, setPrice] = useState(0);
        const [stock, setStock] = useState(1);
        const [sold, setSold] = useState(0);



        function updateProduct(e){
            e.preventDefault()

            fetch(`${process.env.REACT_APP_API_URL}/products/update/${productId}`, {
                method: "PATCH",
                headers: {
                    'Content-Type': "application/json",
                    Authorization : `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({
                    name: name,
                    description: description,
                    category: category,
                    price: price,
                    stock: stock,
                    sold: sold
                })
            })
            .then(res => res.json())
            .then(data => {
                if(data === true){
                    Swal.fire({
                        title: "Successully Update Product",
                        icon: "success",
                        text: `You have successfully Updated product ${name}`
                    })

                    navigate('/admin/allProducts')

                }else {
                    Swal.fire({
                        title: "Duplicate Product Name",
                        icon: "error",
                        text: "Product name is already exist please try another product name"
                    })
                }
            })
            .catch(error => console.log(error))
        };


    return(    
        <Row>
            <Col className='p-2'>
                <Form onSubmit={e => updateProduct(e)} className="addProdForm p-3">
                    <h1 className="text-center addProdWord">Update Product</h1>
                    <Container>
                    <Form.Group className="mb-3" controlId="addProductName">
                        <Form.Label>Product Name</Form.Label>
                        <Form.Control 
                        type="text" 
                        placeholder={name}
                        value={name}
                        onChange={e => setName(e.target.value)}
                        />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="addProductDescription">
                        <Form.Label>Product Description</Form.Label>
                        <Form.Control 
                        type="text" 
                        placeholder={description}
                        value={description}
                        onChange={e => setDescription(e.target.value)}
                        />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="addProductDescription">
                        <Form.Label>Product Type</Form.Label>
                        <Form.Select 
                        aria-label={category} 
                        value={category} 
                        onChange={e => setCategory(e.target.value)}
                        >
                            <option>Select Product Type</option>
                            <option value="Console Game">Console Game</option>
                            <option value="PC Peripherals">PC Peripherals</option>
                            <option value="Handheld Device">Handheld Device</option>
                            <option value="Playstation">Playstation</option>
                            <option value="Xbox">Xbox</option>
                        </Form.Select>
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="addProductPrice">
                        <Form.Label>Price</Form.Label>
                        <Form.Control 
                        type="number" 
                        placeholder={price} 
                        value={price} 
                        onChange={e => setPrice(e.target.value)}
                        />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="addProductStock">
                        <Form.Label>Stock</Form.Label>
                        <Form.Control 
                        type="number" 
                        placeholder={stock} 
                        value={stock} 
                        onChange={e => setStock(e.target.value)} 
                        />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="addProductStock">
                        <Form.Label>Sold</Form.Label>
                        <Form.Control 
                        type="number" 
                        placeholder={sold} 
                        value={sold} 
                        onChange={e => setSold(e.target.value)} 
                        />
                    </Form.Group>
                    <Button variant="success" type="submit">Submit</Button>
                    </Container>
                </Form>
            </Col>
        </Row>
    )    
}
          
