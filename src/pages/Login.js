import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Container } from 'react-bootstrap';
import { Navigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';




export default function Login() {

	const { user, setUser } = useContext(UserContext);
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);


	function loginUser(e){
		e.preventDefault()

	        fetch(`${process.env.REACT_APP_API_URL}/users/login`,{
	            method: "POST",
	            headers: {
	                'Content-Type': 'application/json'
	            },
	            body: JSON.stringify({
	                email: email,
	                password: password
	            })
	        })
	        .then(res => res.json())
	        .then(data => {
	            if(typeof data.access !== "undefined"){
	                localStorage.setItem('token', data.access)
	                retrieveUserDetails(data.access)
	                setEmail("");
	                setPassword("");
	                Swal.fire({
	                    title: "Login Successfull",
	                    icon: "success",
	                    text: `Welcome back!`
	                })
            } else {
                Swal.fire({
                    title: "Authentication Failed",
                    icon: "error",
                    text: `Kindly check your login details and try again!`
                })
            }
        })
        .catch(error => console.log(error))
	    };

    const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setUser({
				id: data._id,
                name: data.name,
                email: data.email,
                isAdmin: data.isAdmin
			})
		})
	};

    useEffect(() => {
        (email !== "" && password !== "") ?
            setIsActive(true) 
            :
            setIsActive(false)
    }, [email, password])

	return(
		(user.isAdmin == true) ?
		    <Navigate to='/admin' /> // <---- /admin on process
		    :
		(user.isAdmin !== null) ?
		    <Navigate to='/' />
		    :
		<Row>
			<Col>
				<Form onSubmit={e => loginUser(e)}>
					<h1 className="text-center my-3" id="loginWord">Login</h1>
					<Container className="loginContainer">
				      	<Form.Group className="mb-3 mt-3" controlId="formBasicEmail">
				        <Form.Label>Email address</Form.Label>
				        <Form.Control 
					        type="email" 
					        placeholder="Enter email"
					        value={email}
					        onChange={e => setEmail(e.target.value)}
					        required 
				        />
				      	</Form.Group>

				      	<Form.Group className="mb-3" controlId="formBasicPassword">
				        <Form.Label>Password</Form.Label>
				        <Form.Control 
					        type="password" 
					        placeholder="Enter Password" 
					        value={password}
					        onChange={e => setPassword(e.target.value)}
					        required 
				        />
				      	</Form.Group>

				      	{
				      		isActive ?
			      				<Button variant="success" type="submit">
			      			  		Submit
			      				</Button>
			      				:
		      					<Button variant="success" type="submit" disabled>
		      				  		Submit
		      					</Button>
				      	}
			    	</Container>
				</Form>
			</Col>
				<label className="text-center">Dont have an account? <a href="/register">Click here</a> to register</label>
		</Row>
	)
}