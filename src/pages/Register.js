import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Container } from 'react-bootstrap';
import { Navigate, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register() {


	const { user } = useContext(UserContext);
	const navigate = useNavigate();

	const [name, setName] = useState("");
	const [lastname, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobile, setMobile] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	const [isActive, setIsActive] = useState(false);

	function registerUser(e){
	        e.preventDefault();

	        fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
	            method: "POST",
	            headers: {
	                'Content-Type': "application/json",
	            },
	            body: JSON.stringify({
	                name: name,
	                email: email,
	                password: password1
	            })
	        })
	        .then(res => res.json())
	        .then(data => {

	            if(data === true){
	                setName("");
	                setLastName("");
	                setEmail("");
	                setPassword1("");
	                setPassword2("");
	                setIsActive(false)
	                Swal.fire({
	                    title: "Successully Register",
	                    icon: "success",
	                    text: "You have successfully registered"
	                })

	                navigate('/login')
	            }else if(data === false){
	                Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again"
					})
	            }else {
	                Swal.fire({
						title: "Email Already Exist",
						icon: "error",
						text: "Email is already exist please try again!"
					})
	            }

	        })
	        .catch(error => console.log(error))
	    };

	    useEffect(() => {
    		if (name !== "" && lastname !== "" && email !== "" && mobile !== "" && mobile.length === 11 && password1 !== "" && password2 !== "" && password1 === password2) 
    		{
      			setIsActive(true);
    		} else {
      			setIsActive(false);
    		}
  		}, [name, lastname, email, mobile, password1, password2]);



	return(
		// (user.isAdmin === true) ?
        //     <Navigate to='' /> --- Admin Page on process
        // :
		<Row>
			<Col>
				<Form className="loginForm" onSubmit={e => registerUser(e)}>
					<h1 className="text-center my-3" id="loginWord">Register</h1>
					<Container className="loginContainer">

						<Form.Group className="mb-3 mt-3" controlId="formBasicEmail">
					  		<Form.Label>First Name</Form.Label>
					  		<Form.Control 
						  		type="text" 
						  		placeholder="Enter First Name"
						  		value={name}
						  		onChange={e => setName(e.target.value)}
	                            required
					  		/>
						</Form.Group>

						<Form.Group className="mb-3 mt-3" controlId="formBasicEmail">
					  		<Form.Label>Last Name</Form.Label>
					  		<Form.Control 
						  		type="text" 
						  		placeholder="Enter Last Name"
						  		value={lastname}
						  		onChange={e => setLastName(e.target.value)}
	                            required

					  		/>
						</Form.Group>

				      	<Form.Group className="mb-3 mt-3" controlId="formBasicEmail">
					        <Form.Label>Email address</Form.Label>
					        <Form.Control 
						        type="email" 
						        placeholder="Enter email"
						        value={email}
						        onChange={e => setEmail(e.target.value)}
						        required 
						    />
				      	</Form.Group>

		      	      	<Form.Group className="mb-3 mt-3" controlId="formBasicEmail">
		      		        <Form.Label>Mobile Number</Form.Label>
		      		        <Form.Control 
		      			        type="Number" 
		      			        placeholder="Enter Mobile Number"
		      			        value={mobile}
						        onChange={e => setMobile(e.target.value)}
						        required  
		      			    />
		      	      	</Form.Group>

				      	<Form.Group className="mb-3" controlId="formBasicPassword">
					        <Form.Label>Password</Form.Label>
					        <Form.Control 
						        type="password" 
						        placeholder="Enter Password"
						        value={password1}
						        onChange={e => setPassword1(e.target.value)}
						        required   
					        />
				      	</Form.Group>

				      	<Form.Group className="mb-3" controlId="formBasicPassword">
			      	  		<Form.Label>Verify Password</Form.Label>
			      	  		<Form.Control 
				      	  		type="password" 
				      	  		placeholder="Verify Password"
				      	  		value={password2}
						        onChange={e => setPassword2(e.target.value)}
						        required  
			      	  		/>
				      	</Form.Group>

				      	{
				      		isActive ?
			      				<Button variant="success" type="submit">
			      			  		Submit
			      				</Button>
			      				:
		      					<Button variant="success" type="submit" disabled>
		      				  		Submit
		      					</Button>
				      	}
			    	</Container>
				</Form>
			</Col>
		</Row>
	)
}