import { useContext } from 'react';
import { Row, Col } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import AdminBanner from '../components/AdminBanner'
import UserContext from '../UserContext';


export default function Admin(){
    const { user } = useContext(UserContext)

    return(
        (user.isAdmin === false) ?
            <Navigate to='/' />
        :
            <>
                <Row className='mt-2'>
                    <Col className='adminBanner mx-1'>
                        <AdminBanner />
                    </Col>
                </Row>
            </>
    )
}